﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour {

    GameObject Settings;
    GameObject Triggers;
    public GameObject TestPopup;

    public void GoToSettings(){
		Settings.SetActive(true);
		Triggers.SetActive(false);
    }

    public void GoToTriggers(){
        Settings.SetActive(false);
        Triggers.SetActive(true);
    }

    public void SetMopubID(string newID){
        
    }

    public void Test(){
        TestPopup.SetActive(!TestPopup.activeSelf);
    }
}
