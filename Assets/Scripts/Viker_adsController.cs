﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
//using GoogleMobileAds.Api;
//using FyberPlugin;

public class Viker_adsController : MonoBehaviour
{
	public delegate void Eventhandler();
	public static event Eventhandler OnVideoAdFinished; // Video watched to the end
	public static event Eventhandler OnVideoAdAborted; // Video interrupted (user close, phone call, app close etc..)
	public static event Eventhandler OnInterstitialClosed;
	public static event Eventhandler OnShowVideoAdFailed; // tried to load a video without a connection or received an error.
	public static event Eventhandler OnShowInterstitialFailed; // tried to load an interstitial without a connection or received an error.
	public static Viker_adsController instance;
	const string AdLoadingScreenPrefabString = "AdLoadingScreen";
	GameObject AdLoadingScreen;

	[Header("MoPub")]
	[SerializeField] string _iOSBanner;
	[SerializeField] string _iOSInterstitial;
	[SerializeField] string _androidBanner;
	[SerializeField] string _androidInterstitial;

	[Header("Fyber Test")]
	[SerializeField] string _testIosAppId;
	[SerializeField] string _testIosToken;
	[SerializeField] string _testAndroidAppId;
	[SerializeField] string _testAndroidToken;

	[Header("Fyber Live")]
	[SerializeField]
	string _liveIosAppId;
	[SerializeField] string _liveIosToken;
	[SerializeField] string _liveAndroidAppId;
	[SerializeField] string _liveAndroidToken;

	[Header("Fyber Settings")]
	[SerializeField]
	bool _runInTestMode;
	[SerializeField] bool _enableFyberLogging;

	[Header("General Settings")]
	[SerializeField]
	float _loadingBufferInSeconds = 10f;
	[SerializeField] float _adTimeoutLimit =5f;
	[SerializeField] string _loadingText = "Loading...";
	[SerializeField] string _loadingFailedText = "Ad failed to load. Please try again.";

	string[] _moPubAdUnits;
	string _fyberAppId;
	string _fyberAppToken;
	//Ad rewardedVideoAd;
	float timeAtGameLoad;
	bool _isVideoShown;
	bool _isInterstitialShown;
	bool _isInterstitialReady;
	bool _isVideoReady;
	bool _isWaitingForAd;
	Text _loadingScreenText;
    IEnumerator _currentWaitingTimer;

	enum AdType
	{
		Interstitial,
		RewardedVideo
	}

	void Awake()
	{
        
		timeAtGameLoad = Time.time;
		instance = this;
#if UNITY_IOS
        _moPubAdUnits = new []{_iOSBanner, _iOSInterstitial};
#elif UNITY_ANDROID
		_moPubAdUnits = new[] { _androidBanner, _androidInterstitial };
#endif
	}

	void Start()
	{
		CreateAdLoadingScreen();
		SignupForAllAdEvents();
#if UNITY_IOS
        MoPub.loadPluginsForAdUnits(_moPubAdUnits);
        _fyberAppId = _runInTestMode ? _testIosAppId : _liveIosAppId;
        _fyberAppToken = _runInTestMode ? _testIosToken : _liveIosToken;
#elif UNITY_ANDROID
		_fyberAppId = _runInTestMode ? _testAndroidAppId : _liveAndroidAppId;
		_fyberAppToken = _runInTestMode ? _testAndroidToken : _liveAndroidToken;
		MoPub.loadBannerPluginsForAdUnits(new[] { _moPubAdUnits[0] });
		MoPub.loadInterstitialPluginsForAdUnits(new[] { _moPubAdUnits[1] });
#endif
		//if (_enableFyberLogging)
		//{
		//	FyberLogger.EnableLogging(true);
		//}
		//Fyber.With(_fyberAppId)
			//.WithSecurityToken(_fyberAppToken)
			//.Start();
	}

	public void ShowInterstitial()
	{
		StartCoroutine(ShowInterstitialSequence());
	}
	//public void ShowVideo()
	//{
	//	StartCoroutine(ShowVideoSequence());
	//}

	public void RequestInterstitial()
	{
		//if (_isInterstitialReady) return;
		_isInterstitialReady = false;
		_isInterstitialShown = false;
		if (!isOnline())
		{
			Debug.LogWarning("Requested an interstitial without an internet connection");
			if (OnShowInterstitialFailed != null)
			{
				OnShowInterstitialFailed();
				return;
			}
		}

		if (!IsLoadingBufferComplete())
		{
			StartCoroutine(WaitForBufferComplete(RequestInterstitial));
			return;
		}
		//MoPub.requestInterstitialAd(_moPubAdUnits[1]);
        print("called request interstitial for " + _moPubAdUnits[1]);
	}

	//public void RequestVideo()
	//{
	//	_isVideoReady = false;
	//	_isVideoShown = false;
	//	if (!isOnline())
	//	{
	//		Debug.LogWarning("Requested a video without an internet connection. Failing.");
	//		if (OnShowVideoAdFailed != null)
	//		{
	//			OnShowVideoAdFailed();
	//		}
	//		return;
	//	}
	//	if (!IsLoadingBufferComplete())
	//	{
	//		StartCoroutine(WaitForBufferComplete(RequestVideo));
	//		return;
	//	}
	//	RewardedVideoRequester.Create()
	//		.NotifyUserOnCompletion(false)
	//		.Request();
	//}

	IEnumerator ShowInterstitialSequence()
	{
		print("is buffer done: " + IsLoadingBufferComplete());
		SetLoadingScreenActive(true);
		_isWaitingForAd = true;
		StartLoadingTimer(AdType.Interstitial);
		while (!_isInterstitialReady)
		{
			yield return new WaitForSeconds(0.5f);
		}
		if (_isWaitingForAd)
		{
			//MoPub.showInterstitialAd(_moPubAdUnits[1]);
		}
		print("called show interstitial for " + _moPubAdUnits[1]);
	}

	//IEnumerator ShowVideoSequence()
	//{
	//	SetLoadingScreenActive(true);
	//	_isWaitingForAd = true;
	//	StartLoadingTimer(AdType.RewardedVideo);
	//	while (!_isVideoReady)
	//	{
	//		yield return new WaitForSeconds(0.5f);
	//	}
	//	if (rewardedVideoAd != null && _isWaitingForAd)
	//	{
	//		print("Show video. User is waiting.");
	//		rewardedVideoAd.Start();
	//		rewardedVideoAd = null;
	//	}
	//}

	IEnumerator WaitForBufferComplete(Action OnComplete)
	{
		while (!IsLoadingBufferComplete())
		{
			//print("not passed buffer yet.");
			yield return new WaitForSeconds(0.2f);
		}
		print("buffer passed. Continue.");
		OnComplete();
	}

	public void HideBanner()
	{
		//MoPub.destroyBanner(_moPubAdUnits[0]);
	}

	int numberOfBannersRequested = 0;

	public void ShowBanner()
	{
		if (!isOnline())
		{
			Debug.LogWarning("Trying to show a banner without an internet connection. Didn't proceed");
			return;
		}

  //      BannerView bannerView = new BannerView(_iOSBanner, AdSize.SmartBanner, AdPosition.Bottom);
		//AdRequest request = new AdRequest.Builder().Build();
		//bannerView.LoadAd(request);

		MoPub.createBanner(_moPubAdUnits[0], MoPubAdPosition.BottomCenter);
		numberOfBannersRequested++;
		print("Number of banners requested: " + numberOfBannersRequested);
	}

	#region Event Handling

	public void OnFyberError(string message)
	{
		print("Fyber error received. Message: " + message);
	}

	void onInterstitialLoaded(string adUnitId)
	{
		Debug.Log("interstial loaded for " + adUnitId);
		_isInterstitialReady = true;
		_isInterstitialShown = false;
	}

	void onInterstitialDismissed(string adUnitId)
	{
		if (OnInterstitialClosed != null)
		{
			OnInterstitialClosed();
		}
		_isInterstitialReady = false;
		Debug.Log("interstial dismissed: " + adUnitId);
        StopCoroutine(_currentWaitingTimer);
		SetLoadingScreenActive(false);
	}

	void onInterstitialFailed(string errorMsg)
	{
		print("interstitial failed. reason: " + errorMsg);
		if (OnShowInterstitialFailed != null)
		{
			OnShowInterstitialFailed();
		}
		_isInterstitialReady = false;
	}

	void onInterstitialShown(string adUnitId)
	{
		//      if (OnInterstitialClosed != null)
		//      {
		//          OnInterstitialClosed();
		//      }
		_isInterstitialShown = true;
		_isInterstitialReady = false;
		print("interstitial shown: " + adUnitId);
		StopCoroutine(_currentWaitingTimer);

		//      SetLoadingScreenActive(false);
	}

	void onBannerAdLoadSuccess(float height)
	{
		print("banner ad load success. height: " + height);
	}

	void onBannerAdLoadFail(string message)
	{
		print("banner ad load fail. Message: " + message);
	}

	//void OnAdAvailable(Ad ad)
	//{
	//	// store ad response
	//	if (ad.AdFormat == AdFormat.REWARDED_VIDEO)
	//		print("fyber video received and available.");
	//	_isVideoReady = true;
	//	_isVideoShown = false;
	//	rewardedVideoAd = ad;
	//}

	//void OnAdNotAvailable(AdFormat adFormat)
	//{
	//	if (adFormat == AdFormat.REWARDED_VIDEO)
	//		print("fyber video not available");
	//	rewardedVideoAd = null;
	//	if (OnShowVideoAdFailed != null)
	//	{
	//		OnShowVideoAdFailed();
	//	}
	//	_isVideoReady = false;
	//}

	//void OnRequestFail(RequestError error)
	//{
	//	// process error
	//	Debug.Log("OnFyberRequestError: " + error.Description);
	//}

	//void OnVideoStarted(Ad ad)
	//{
	//	_isVideoShown = true;
 //       StopCoroutine(_currentWaitingTimer);	
 //   }

	//void OnVideoFinished(AdResult result)
	//{
	//	/* 
 //       result.message can be: 
 //       "CLOSE_FINISHED" =  The video has finished after completing.
 //       "CLOSE_ABORTED" = The video has finished before completing. 
 //       The user might have aborted it, either explicitly (by tapping the close button) 
 //       or implicitly (by switching to another app) or it was interrupted by 
 //       an asynchronous event like an incoming phone call.
 //       "ERROR" = The video was interrupted or failed to play due to an error.
 //       */
	//	if (result.AdFormat == AdFormat.REWARDED_VIDEO)
	//	{
	//		Debug.Log("Rewarded video closed with result: " + result.Status +
	//				  " and message: " + result.Message);
	//		if (result.Message == "CLOSE_FINISHED")
	//		{
	//			if (OnVideoAdFinished != null)
	//			{
	//				OnVideoAdFinished();
	//			}
	//			StopCoroutine(_currentWaitingTimer);
	//			SetLoadingScreenActive(false);

	//		}
	//		else if (result.Message == "CLOSE_ABORTED")
	//		{
	//			if (OnVideoAdAborted != null)
	//			{
	//				OnVideoAdAborted();
	//			}
	//			SetLoadingScreenActive(false);
	//			StopCoroutine(_currentWaitingTimer);
	//		}
	//	}
	//	_isVideoReady = false;
	//}

	IEnumerator LoadingTimerSequence(AdType adType)
	{
		float timeElapsed = 0f;
		print("waiting for ad...");
		while (timeElapsed < _adTimeoutLimit)
		{
			timeElapsed += 1f;
			print(timeElapsed);
			yield return new WaitForSeconds(1f);
		}
		// check for interstitial and video shown.
		if (adType == AdType.Interstitial)
		{
			if (!_isInterstitialShown)
			{
				print("interstitial didn't show");
				SetLoadingFailedText();
				StartCoroutine(HideLoadingScreenWithDelay());
				if (OnShowInterstitialFailed != null)
				{
					OnShowInterstitialFailed();
					print("triggered on show interstitial failed");
				}
			}
			else
			{
				print("Interstitial was shown.");
			}
		}
		else if (adType == AdType.RewardedVideo)
		{
			if (!_isVideoShown)
			{
				SetLoadingFailedText();
				StartCoroutine(HideLoadingScreenWithDelay());
				print("video didn't show");
				if (OnVideoAdAborted != null)
				{
					OnVideoAdAborted();
					print("triggered on show video failed");
				}
			}
			else
			{
				print("video was shown.");
			}
		}
		_isWaitingForAd = false;
	}

	#endregion

	void CreateAdLoadingScreen()
	{
		AdLoadingScreen = Instantiate(Resources.Load(AdLoadingScreenPrefabString), Camera.main.transform.parent, false) as GameObject;
		AdLoadingScreen.transform.SetAsLastSibling();
		_loadingScreenText = AdLoadingScreen.GetComponentInChildren<Text>();
	}

	void SignupForAllAdEvents()
	{
		MoPubManager.onInterstitialLoadedEvent += onInterstitialLoaded;
		MoPubManager.onInterstitialDismissedEvent += onInterstitialDismissed;
		MoPubManager.onInterstitialShownEvent += onInterstitialShown;
		MoPubManager.onInterstitialFailedEvent += onInterstitialFailed;
		MoPubManager.onAdLoadedEvent += onBannerAdLoadSuccess;
		MoPubManager.onAdFailedEvent += onBannerAdLoadFail;
	//	FyberCallback.NativeError += OnFyberError;
	//	FyberCallback.AdAvailable += OnAdAvailable;
	//	FyberCallback.AdNotAvailable += OnAdNotAvailable;
	//	FyberCallback.RequestFail += OnRequestFail;
	//	FyberCallback.AdFinished += OnVideoFinished;
	//	FyberCallback.AdStarted += OnVideoStarted;
	}

	void OnDisable()
	{
		MoPubManager.onInterstitialLoadedEvent -= onInterstitialLoaded;
		MoPubManager.onInterstitialDismissedEvent -= onInterstitialDismissed;
		MoPubManager.onInterstitialShownEvent -= onInterstitialShown;
		MoPubManager.onInterstitialFailedEvent -= onInterstitialFailed;
		MoPubManager.onAdLoadedEvent -= onBannerAdLoadSuccess;
		MoPubManager.onAdFailedEvent -= onBannerAdLoadFail;
		//FyberCallback.NativeError -= OnFyberError;
		//FyberCallback.AdAvailable -= OnAdAvailable;
		//FyberCallback.AdNotAvailable -= OnAdNotAvailable;
		//FyberCallback.RequestFail -= OnRequestFail;
		//FyberCallback.AdFinished -= OnVideoFinished;
		//FyberCallback.AdStarted -= OnVideoStarted;
	}

	//public void TestFyber()
	//{
	//	IntegrationAnalyzer.ShowTestSuite();
	//}


	bool isOnline()
	{
		return Application.internetReachability != NetworkReachability.NotReachable;
	}

	bool IsLoadingBufferComplete()
	{
		return Time.time - timeAtGameLoad > _loadingBufferInSeconds;
	}

	void SetLoadingScreenActive(bool isActive)
	{
		if (isActive)
		{
			SetLoadingText();
		}
		AdLoadingScreen.SetActive(isActive);
	}

	void SetLoadingText()
	{
		_loadingScreenText.text = _loadingText;
	}

	void SetLoadingFailedText()
	{
		_loadingScreenText.text = _loadingFailedText;
	}

	IEnumerator HideLoadingScreenWithDelay()
	{
		yield return new WaitForSeconds(2f);
		SetLoadingScreenActive(false);
	}

	void StartLoadingTimer(AdType adtype)
	{
        _currentWaitingTimer = LoadingTimerSequence(adtype);
        StartCoroutine(_currentWaitingTimer);
	}
}